package set

// Set represents a generic set of elements.
type Set[T comparable] map[T]struct{}

// FromMapKeys creates a new set from a map's keys.
func FromMapKeys[Key comparable, Val any](m map[Key]Val) Set[Key] {
	set := make(Set[Key], len(m))
	for key := range m {
		set.Add(key)
	}
	return set
}

// FromMapVals creates a new set from a map's values.
func FromMapVals[Key comparable, Val comparable](m map[Key]Val) Set[Val] {
	set := make(Set[Val], len(m))
	for _, val := range m {
		set.Add(val)
	}
	return set
}

// FromSlice creates a new set from a slice.
func FromSlice[T comparable](slice []T) Set[T] {
	set := make(Set[T], len(slice))
	for _, element := range slice {
		set.Add(element)
	}
	return set
}

// ToSlice creates a new slice from a set.
func (s Set[T]) ToSlice() []T {
	slice := make([]T, 0, len(s))
	for element := range s {
		slice = append(slice, element)
	}
	return slice
}

// Add adds an element to the set.
func (s Set[T]) Add(element T) {
	s[element] = struct{}{}
}

// Remove removes an element from the set.
func (s Set[T]) Remove(element T) bool {
	_, found := s[element]
	delete(s, element)
	return found
}

// RemoveEmptyValue removes the uninitialized value of T from the set.
func (s Set[T]) RemoveEmptyValue() bool {
	var val T
	return s.Remove(val)
}

// Contains checks if the set contains all passed elements.
func (s Set[T]) Contains(elements ...T) bool {
	for _, element := range elements {
		if _, found := s[element]; !found {
			return false
		}
	}
	return true
}

// IsEmpty checks if the set is empty.
func (s Set[T]) IsEmpty() bool {
	return len(s) == 0
}

// Copy creates a copy of the set.
func (s Set[T]) Copy() Set[T] {
	copySet := make(Set[T], len(s))
	for element := range s {
		copySet.Add(element)
	}
	return copySet
}

// IsSubsetOf checks if the set is a subset of another set.
func (s Set[T]) IsSubsetOf(other Set[T]) bool {
	for element := range s {
		if !other.Contains(element) {
			return false
		}
	}
	return true
}

// Union returns a new set that is the union of multiple sets.
func Union[T comparable](sets ...Set[T]) Set[T] {
	var size int
	if len(sets) != 0 {
		size = len(sets[0]) * len(sets)
	}
	unionSet := make(Set[T], size)
	for _, set := range sets {
		for element := range set {
			unionSet.Add(element)
		}
	}
	return unionSet
}

// Intersection returns a new set that is the intersection of multiple sets.
func Intersection[T comparable](sets ...Set[T]) Set[T] {
	if len(sets) == 0 {
		return make(Set[T])
	}

	intersectionSet := sets[0].Copy()
	for _, set := range sets[1:] {
		for element := range intersectionSet {
			if !set.Contains(element) {
				intersectionSet.Remove(element)
			}
		}
	}

	return intersectionSet
}

// Difference returns a new set that is the difference of two or more sets (set1 - set2 - ... - setN).
func Difference[T comparable](minuend Set[T], subtrahends ...Set[T]) Set[T] {
	difference := minuend.Copy()
	if len(subtrahends) == 0 {
		return difference
	}

	for _, subtrahend := range subtrahends {
		for element := range subtrahend {
			difference.Remove(element)
		}
	}

	return difference
}
